import numpy as np
import matplotlib.pyplot as plt

# Image width and height; parameters for the plot
im_width, im_height = 4800, 4800
#c = complex(-0.123,0.745) #rabbit
#c = complex(-1,0) #basilica
#c = complex(-1.755,0) #airplane
c = complex(-1.31,0) #quasi-rabbit
#c = complex(0.38,-0.34) #5-rabbit
zabs_max = 2 #escape radius
nit_max = 110 #Maximal number of iterations: 50 for the 3-rabbit and the basilica, 20 for the airplane and 110 for the 5-rabbit
xmin, xmax = -2, 2
xwidth = xmax - xmin
ymin, ymax = -2, 2
yheight = ymax - ymin

#Computation of the filled in Julia set
filled_in_julia = np.zeros((im_width, im_height))
for ix in range(im_width):
    for iy in range(im_height):
        nit = 0
        # Map pixel position to a point in the complex plane
        z = complex(ix / im_width * xwidth + xmin,
                    iy / im_height * yheight + ymin)
        # Do the iterations
        while abs(z) <= zabs_max and nit < nit_max:
            z = z**2 + c
            nit += 1
        if nit==nit_max:
            filled_in_julia[iy,ix] = 1
            
#Computation of the Julia set as border of the filled in Julia set          
julia = np.zeros((im_width, im_height))
for ix in range(1,im_width-1):
    for iy in range(1,im_height-1):
        if filled_in_julia[iy,ix]==1:
            for x in range(-1,2):
                for y in range(-1,2):
                    if filled_in_julia[iy+y,ix+x]==0:
                        julia[iy,ix]=1
                        break
                        
#Thickning step for better looking of the Julia set                        
julia_thicked = np.zeros((im_width,im_height))  
for ix in range(im_width-1):
    for iy in range(im_height-1):
        for x in range(2):
            for y in range(2):
                if julia[iy+y,ix+x]==1:
                    julia_thicked[iy,ix]=1
                    break

plt.imshow(julia_thicked, interpolation='nearest', cmap='Greys')
plt.axis('off')
plt.savefig('julia_quasi_rabbit.png', dpi=1000)
plt.show()