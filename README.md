# Display Julia sets in Python

## Context

Julia sets are ... The goal here is display Julia sets for second order polynomials $f_c(z)=z^2+c$ where $c$ is a complex number. One can find many places on the web with code to display filled in Julia sets but not many to display only the Julia set in a black and white style.

## Description of the script

The script has three small steps.

1. Compute the filled in Julia set
2. Compute a discrete boundary as a proxy for the Julia set (a black point is considered to be interior of all neighbors are black as well)
3. Thickning of the drawing (this is an aesthetic step)

## Remarks

The size of the part of the plane represented and the escape radius are to be adapted depending on $c$. This can be estimated a priori.

The maximal number of iterations should be choosen depending on $c$. This number gives more or less good looking pictures.

## Examples

### Douady Rabbit
![Douady Rabbit](julia_rabbit.png)

### Basilica
![Basilica](julia_basilica.png)

### 5-regular rabbit
![5-regular rabbit](julia_5_rabbit.png)
